--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: angestellter; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE angestellter (
    persnr integer NOT NULL,
    gehalt real,
    ueberstunden integer,
    e_mail character varying(50)
);


ALTER TABLE public.angestellter OWNER TO postgres;

--
-- Name: aufzeichnung; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE aufzeichnung (
    datum date NOT NULL,
    bezeichnung character varying(30) NOT NULL,
    gegner character(10),
    ergebnis character(20)
);


ALTER TABLE public.aufzeichnung OWNER TO postgres;

--
-- Name: beteiligt; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE beteiligt (
    datum date NOT NULL,
    bezeichnung character varying(30) NOT NULL,
    persnr integer NOT NULL,
    dauer character(10)
);


ALTER TABLE public.beteiligt OWNER TO postgres;

--
-- Name: betreut; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE betreut (
    name character varying(50) NOT NULL,
    sid integer NOT NULL,
    persnr integer NOT NULL,
    anfang date,
    ende date
);


ALTER TABLE public.betreut OWNER TO postgres;

--
-- Name: fanclub; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE fanclub (
    name character varying(50) NOT NULL,
    sid integer NOT NULL,
    obmann integer,
    gegrundet date
);


ALTER TABLE public.fanclub OWNER TO postgres;

--
-- Name: manschaft; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE manschaft (
    bezeichnung character varying(30) NOT NULL,
    klasse character(10),
    cheftrainer integer,
    assistentrainer integer,
    kaptain integer,
    naechstes_spiel date
);


ALTER TABLE public.manschaft OWNER TO postgres;

--
-- Name: person; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE person (
    persnr integer NOT NULL,
    vname character varying(20),
    nname character varying(20),
    geschlecht character(1),
    gebdat date
);


ALTER TABLE public.person OWNER TO postgres;

--
-- Name: person_persnr_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE person_persnr_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_persnr_seq OWNER TO postgres;

--
-- Name: person_persnr_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE person_persnr_seq OWNED BY person.persnr;


--
-- Name: spieler; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE spieler (
    persnr integer NOT NULL,
    "position" character(10),
    gehalt character(10),
    von date,
    bis date
);


ALTER TABLE public.spieler OWNER TO postgres;

--
-- Name: spieltin; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE spieltin (
    persnr integer NOT NULL,
    bezeichnung character varying(30) NOT NULL,
    nummer character(10)
);


ALTER TABLE public.spieltin OWNER TO postgres;

--
-- Name: standort; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE standort (
    sid integer NOT NULL,
    land character varying(20),
    plz character varying(10),
    ort character varying(30)
);


ALTER TABLE public.standort OWNER TO postgres;

--
-- Name: standort_sid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE standort_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.standort_sid_seq OWNER TO postgres;

--
-- Name: standort_sid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE standort_sid_seq OWNED BY standort.sid;


--
-- Name: trainer; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE trainer (
    persnr integer NOT NULL,
    gehalt character(10)
);


ALTER TABLE public.trainer OWNER TO postgres;

--
-- Name: vereinsmitglied; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vereinsmitglied (
    persnr integer NOT NULL,
    beitrag real
);


ALTER TABLE public.vereinsmitglied OWNER TO postgres;

--
-- Name: persnr; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY person ALTER COLUMN persnr SET DEFAULT nextval('person_persnr_seq'::regclass);


--
-- Name: sid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY standort ALTER COLUMN sid SET DEFAULT nextval('standort_sid_seq'::regclass);


--
-- Name: pk_angestellter; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY angestellter
    ADD CONSTRAINT pk_angestellter PRIMARY KEY (persnr);


--
-- Name: pk_aufzeichnung; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY aufzeichnung
    ADD CONSTRAINT pk_aufzeichnung PRIMARY KEY (datum, bezeichnung);


--
-- Name: pk_beteiligt; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY beteiligt
    ADD CONSTRAINT pk_beteiligt PRIMARY KEY (datum, bezeichnung, persnr);


--
-- Name: pk_betreut; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY betreut
    ADD CONSTRAINT pk_betreut PRIMARY KEY (name, sid, persnr);


--
-- Name: pk_fanclub; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY fanclub
    ADD CONSTRAINT pk_fanclub PRIMARY KEY (name, sid);


--
-- Name: pk_manschaft; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY manschaft
    ADD CONSTRAINT pk_manschaft PRIMARY KEY (bezeichnung);


--
-- Name: pk_person; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY person
    ADD CONSTRAINT pk_person PRIMARY KEY (persnr);


--
-- Name: pk_spieler; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY spieler
    ADD CONSTRAINT pk_spieler PRIMARY KEY (persnr);


--
-- Name: pk_spieltin; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY spieltin
    ADD CONSTRAINT pk_spieltin PRIMARY KEY (persnr, bezeichnung);


--
-- Name: pk_standort; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY standort
    ADD CONSTRAINT pk_standort PRIMARY KEY (sid);


--
-- Name: pk_trainer; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY trainer
    ADD CONSTRAINT pk_trainer PRIMARY KEY (persnr);


--
-- Name: pk_vereinsmitglied; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vereinsmitglied
    ADD CONSTRAINT pk_vereinsmitglied PRIMARY KEY (persnr);


--
-- Name: fk_angestellter_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY angestellter
    ADD CONSTRAINT fk_angestellter_0 FOREIGN KEY (persnr) REFERENCES person(persnr);


--
-- Name: fk_aufzeichnung_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aufzeichnung
    ADD CONSTRAINT fk_aufzeichnung_0 FOREIGN KEY (bezeichnung) REFERENCES manschaft(bezeichnung);


--
-- Name: fk_beteiligt_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY beteiligt
    ADD CONSTRAINT fk_beteiligt_0 FOREIGN KEY (datum, bezeichnung) REFERENCES aufzeichnung(datum, bezeichnung);


--
-- Name: fk_beteiligt_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY beteiligt
    ADD CONSTRAINT fk_beteiligt_1 FOREIGN KEY (persnr) REFERENCES spieler(persnr);


--
-- Name: fk_betreut_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY betreut
    ADD CONSTRAINT fk_betreut_0 FOREIGN KEY (name, sid) REFERENCES fanclub(name, sid);


--
-- Name: fk_betreut_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY betreut
    ADD CONSTRAINT fk_betreut_1 FOREIGN KEY (persnr) REFERENCES angestellter(persnr);


--
-- Name: fk_fanclub_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fanclub
    ADD CONSTRAINT fk_fanclub_0 FOREIGN KEY (sid) REFERENCES standort(sid);


--
-- Name: fk_fanclub_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fanclub
    ADD CONSTRAINT fk_fanclub_1 FOREIGN KEY (obmann) REFERENCES vereinsmitglied(persnr);


--
-- Name: fk_manschaft_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manschaft
    ADD CONSTRAINT fk_manschaft_0 FOREIGN KEY (cheftrainer) REFERENCES trainer(persnr);


--
-- Name: fk_manschaft_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manschaft
    ADD CONSTRAINT fk_manschaft_1 FOREIGN KEY (assistentrainer) REFERENCES trainer(persnr);


--
-- Name: fk_manschaft_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manschaft
    ADD CONSTRAINT fk_manschaft_3 FOREIGN KEY (kaptain) REFERENCES spieler(persnr);


--
-- Name: fk_spieler_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY spieler
    ADD CONSTRAINT fk_spieler_0 FOREIGN KEY (persnr) REFERENCES person(persnr);


--
-- Name: fk_spieltin_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY spieltin
    ADD CONSTRAINT fk_spieltin_0 FOREIGN KEY (persnr) REFERENCES spieler(persnr);


--
-- Name: fk_spieltin_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY spieltin
    ADD CONSTRAINT fk_spieltin_1 FOREIGN KEY (bezeichnung) REFERENCES manschaft(bezeichnung);


--
-- Name: fk_trainer_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY trainer
    ADD CONSTRAINT fk_trainer_0 FOREIGN KEY (persnr) REFERENCES person(persnr);


--
-- Name: fk_vereinsmitglied_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vereinsmitglied
    ADD CONSTRAINT fk_vereinsmitglied_0 FOREIGN KEY (persnr) REFERENCES person(persnr);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

