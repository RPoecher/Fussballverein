
--1--


SELECT 
  fanclub.sid,
  fanclub.name
  FROM public.fanclub WHERE fanclub.sid NOT IN (
SELECT 
  fanclub.sid
FROM 
  public.fanclub left JOIN
  public.betreut ON (fanclub.sid = betreut.sid)
  WHERE 
  betreut.anfang < current_date and
  betreut.ende > current_date and
  betreut.persnr = 5)



--2--

SELECT 
  person.nname, 
  person.persnr
FROM 
  public.betreut, 
  public.angestellter, 
  public.person
WHERE 
  betreut.persnr = person.persnr AND
  angestellter.persnr = betreut.persnr
GROUP BY
  person.nname,
  person.persnr
HAVING
  COUNT(betreut.sid) = (
    SELECT COUNT(*)
    FROM betreut
  )
ORDER BY
  person.nname
;

--3--


SELECT 
  spieltin.bezeichnung, 
  person.vname, 
  person.nname, 
  beteiligt.dauer, 
  beteiligt.datum
FROM
  public.person, 
  public.beteiligt,
  public.spieltin
WHERE 
  beteiligt.persnr = person.persnr AND
  spieltin.persnr = person.persnr AND
  beteiligt.bezeichnung = spieltin.bezeichnung AND
  beteiligt.datum >= '2015-01-01' AND 
  beteiligt.datum < '2016-01-01';



--4--

SELECT vname, nname, temp1.dauer
FROM (
  SELECT persnr, sum(dauer::int) AS "dauer"
  FROM beteiligt
  GROUP BY persnr
  UNION
  SELECT persnr, 0
  FROM spieler
  WHERE persnr NOT IN(
    SELECT DISTINCT persnr
    FROM beteiligt
  )
) as temp1,
person,
beteiligt
WHERE
  person.persnr = temp1.persnr AND
  person.persnr = beteiligt.persnr AND
  beteiligt.datum >= '2015-01-01' AND 
  beteiligt.datum < '2016-01-01'
ORDER BY nname

--5--

SELECT nname, vname
FROM beteiligt, person
WHERE beteiligt.persnr = person.persnr AND
  beteiligt.datum >= '2015-01-01' AND 
  beteiligt.datum < '2016-01-01'
GROUP BY beteiligt.persnr, nname, vname
HAVING sum(dauer::int) = (
  SELECT MAX(dauer) FROM (
    SELECT persnr, sum(dauer::int) AS "dauer"
    FROM beteiligt
    WHERE beteiligt.datum >= '2015-01-01' AND 
      beteiligt.datum < '2016-01-01'
    GROUP BY persnr
  ) as temp1
)
ORDER BY nname;



