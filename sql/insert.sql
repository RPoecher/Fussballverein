--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY person (persnr, vname, nname, geschlecht, gebdat) FROM stdin;
1	rene	poecher	m	1995-08-11
2	david	sindl	m	1111-11-11
3	sdas	dsadfas	m	1111-11-11
4	swdfws	dwqda	w	1111-11-11
5	dsadas	sadas	m	1111-11-11
6	sqa	saxsd	w	1111-11-11
7	uzjzj	ztjtz	w	1111-11-11
8	sadas	sdfs	m	1111-11-11
9	ewfwef	ewfwef	w	1111-11-11
10	dwqdw	sdcsd	w	1111-11-11
11	qwdwqsa	wdasd	w	1111-11-11
12	sadsad	dsad	w	1111-11-11
13	regerg	erger	w	1111-11-11
14	fewfw	wefwe	w	1111-11-11
15	z5trg	rgrt	w	1111-11-11
\.


--
-- Data for Name: angestellter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY angestellter (persnr, gehalt, ueberstunden, e_mail) FROM stdin;
10	12	12	12
5	12	12	
\.


--
-- Data for Name: spieler; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY spieler (persnr, "position", gehalt, von, bis) FROM stdin;
1	sturm     	5         	1111-11-11	2222-11-11
2	sturm     	5         	1111-11-11	2222-11-11
\.


--
-- Data for Name: trainer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY trainer (persnr, gehalt) FROM stdin;
3	10        
4	2         
\.


--
-- Data for Name: manschaft; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY manschaft (bezeichnung, klasse, cheftrainer, assistentrainer, kaptain, naechstes_spiel) FROM stdin;
Go 4AHITM	Profi     	3	4	1	2222-11-11
\.


--
-- Data for Name: aufzeichnung; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aufzeichnung (datum, bezeichnung, gegner, ergebnis) FROM stdin;
3333-11-11	Go 4AHITM	hm        	/                   
\.


--
-- Data for Name: beteiligt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY beteiligt (datum, bezeichnung, persnr, dauer) FROM stdin;
3333-11-11	Go 4AHITM	1	30        
3333-11-11	Go 4AHITM	2	2         
\.


--
-- Data for Name: standort; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY standort (sid, land, plz, ort) FROM stdin;
1	Testland	1210	Testort
2	testland1	1211	testort1
3	sdfsdf	esffse	efsef
4	gdf	sfsdsf	sdgds
\.


--
-- Data for Name: vereinsmitglied; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY vereinsmitglied (persnr, beitrag) FROM stdin;
11	123
\.


--
-- Data for Name: fanclub; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY fanclub (name, sid, obmann, gegrundet) FROM stdin;
TestName	1	11	1111-11-11
Thomas ist cool	2	11	1111-11-11
Ich hasse Fussball	3	11	1111-11-11
hmmm	4	11	1111-11-11
\.


--
-- Data for Name: betreut; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY betreut (name, sid, persnr, anfang, ende) FROM stdin;
TestName	1	10	2000-11-11	3000-11-11
Thomas ist cool	2	5	2222-11-11	3000-11-11
Ich hasse Fussball	3	5	2000-11-11	3000-11-11
TestName	1	5	1000-01-01	2000-02-02
\.


--
-- Name: person_persnr_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('person_persnr_seq', 15, true);


--
-- Data for Name: spieltin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY spieltin (persnr, bezeichnung, nummer) FROM stdin;
\.


--
-- Name: standort_sid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('standort_sid_seq', 4, true);


--
-- PostgreSQL database dump complete
--

