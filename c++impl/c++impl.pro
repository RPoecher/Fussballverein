#-------------------------------------------------
#
# Project created by QtCreator 2016-04-16T18:40:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = c++impl
TEMPLATE = app

SOURCES += main.cpp


QMAKE_CXXFLAGS += -std=c++11

HEADERS +=


unix|win32: LIBS += -lpqxx

LIBS += \
       -lboost_system\
       -lrt
