#include "mainwindow.h"
#include <QApplication>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QTableView>
#include <mutex>
#include <unistd.h>
#include <iostream>
#include <thread>
#include <pqxx/pqxx>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>


using namespace pqxx;
using namespace std;
using namespace boost::interprocess;

connection *connectiondb;
result res;
string ipeing, usereing, passworteing, porteing, dbeing;
mutex m ;
QString s;
 QStandardItem coni;

void updatecons(){


    // shared memory auf 'shres' setzen
    shared_memory_object shresc(open_or_create, "shres", boost::interprocess::read_write);
    // setting the size of the shared memory
    shresc.truncate (1024);

    // Mapping zu Adressraum des Prozesses erstellen
    mapped_region regionc { shresc , boost::interprocess::read_write };

    while(true)
       {

        int * ic = static_cast <int*>(regionc.get_address());

        s = QString::number(*ic);
        coni.setText(s);

           usleep(1000);


       }




}


void disconnectdb(){

   connectiondb->disconnect();
}

void connectdb(){

    //connection aufbauen

    connectiondb = new connection("dbname="+dbeing+" user="+usereing+" password="+passworteing+" hostaddr="+ipeing+" port="+porteing);
    if (connectiondb->is_open()) {
       cout << "Opened database successfully: " << connectiondb->dbname() << endl;
    } else {
       cout << "Can't open database" << endl;
    }

}

void getdaten(string sql){

         lock_guard < mutex > lock ( m ) ; // m sperren

         work txn(*connectiondb);
         /* Execute SQL query */
         res= txn.exec(sql);



}



int main(int argc, char *argv[])
{


    QApplication a(argc, argv);

    //anmelde table
    QTableView view1;
    QStandardItemModel model1(0,6);


    view1.setModel(&model1);

      model1.setHeaderData(0, Qt::Horizontal, QObject::tr("Start"));
      model1.setHeaderData(1, Qt::Horizontal, QObject::tr("IP"));
      model1.setHeaderData(2, Qt::Horizontal, QObject::tr("Benutzer"));
      model1.setHeaderData(3, Qt::Horizontal, QObject::tr("Passwort"));
      model1.setHeaderData(4, Qt::Horizontal, QObject::tr("Datenbank"));
      model1.setHeaderData(5, Qt::Horizontal, QObject::tr("Port"));

      QStandardItem start("<Click to Connect>" );
      start.setEditable(false);

      QStandardItem ip("127.0.0.1" );
      QStandardItem benutzer("postgres");
      QStandardItem datenbank("fb" );
      QStandardItem port("5432" );
      QStandardItem passwort("iamblue" );


      model1.insertRow(0, &start);
      model1.setItem(0,1,&ip);
      model1.setItem(0,2,&benutzer);
      model1.setItem(0,3,&passwort);
      model1.setItem(0,4,&datenbank);
      model1.setItem(0,5,&port);



      view1.show();
     //annmelde table



      //close

      // shared memory auf 'shres' setzen
      shared_memory_object shres(open_or_create, "shres", boost::interprocess::read_write);
      // setting the size of the shared memory
      shres.truncate (1024);

      // Mapping zu Adressraum des Prozesses erstellen
      mapped_region region { shres , boost::interprocess::read_write };

      int * i = static_cast <int*>(region.get_address());
      * i = *i+1;

      int * i1 = static_cast <int*>(region.get_address());
      cout << " Connectet : " << * i1 << endl ;

      s = QString::number(*i);
      coni.setText(s);


      thread updatec(updatecons);


      QTableView cTable;
      QStandardItemModel cModel(1, 2 );

      cModel.setHeaderData(0, Qt::Horizontal, QObject::tr("Closing"));
      cModel.setHeaderData(1, Qt::Horizontal, QObject::tr("Current"));

      cTable.setModel(&cModel);


      QStandardItem closec("<Click to Close>" );
      start.setEditable(false);

      cModel.setItem(0,0,&closec);
      cModel.setItem(0,1,&coni);

      cTable.show();


      QApplication::connect(&cTable, &QTableView::doubleClicked,
              [&cModel,&a](const QModelIndex index)
      {

          // shared memory auf 'shres' setzen
          shared_memory_object shres1(open_or_create, "shres", boost::interprocess::read_write);
          // setting the size of the shared memory
          shres1.truncate (1024);

          // Mapping zu Adressraum des Prozesses erstellen
          mapped_region region1 { shres1 , boost::interprocess::read_write };

          int * i2 = static_cast < int * >(region1.get_address());
          * i2 = *i2-1;

          int * i3 = static_cast < int * >(region1.get_address());
          cout << " Connectet : " << * i3 << endl;


          return a.closeAllWindows();

      });


      //close




    //Personen Table
    QTableView personTable;
    QStandardItemModel personModel(0, 5 );
    QStandardItem insertRow("<Click to add new item>" );



    personModel.setHeaderData(0, Qt::Horizontal, QObject::tr("ID"));
    personModel.setHeaderData(1, Qt::Horizontal, QObject::tr("Position"));
    personModel.setHeaderData(2, Qt::Horizontal, QObject::tr("Gehalt"));
    personModel.setHeaderData(3, Qt::Horizontal, QObject::tr("Von"));
    personModel.setHeaderData(4, Qt::Horizontal, QObject::tr("Bis"));



    insertRow.setEditable(false);

    personTable.setModel(&personModel);
    personModel.insertRow(0, &insertRow);


    QApplication::connect(&personTable, &QTableView::doubleClicked,
            [&personModel](const QModelIndex index)
    {
        int rowCount = personModel.rowCount();
        if (index.row() == rowCount - 1)
            personModel.insertRow(rowCount - 1, new QStandardItem{ "New item" });
    });
    //Personen Table



    //Zeittable
    QTableView zeitTable;
    QStandardItemModel zeitModel(0, 4 );

    zeitModel.setHeaderData(0, Qt::Horizontal, QObject::tr("Datum"));
    zeitModel.setHeaderData(1, Qt::Horizontal, QObject::tr("Mannschaft"));
    zeitModel.setHeaderData(2, Qt::Horizontal, QObject::tr("SpielerID"));
    zeitModel.setHeaderData(3, Qt::Horizontal, QObject::tr("Dauer"));


    insertRow.setEditable(false);

    zeitTable.setModel(&zeitModel);
    zeitModel.insertRow(0, insertRow.clone());


    QApplication::connect(&zeitTable, &QTableView::doubleClicked,
            [&zeitModel](const QModelIndex index)
    {
        int rowCount = zeitModel.rowCount();
        if (index.row() == rowCount - 1)
            zeitModel.insertRow(rowCount - 1, new QStandardItem{ "New item" });
    });


    //Zeittable




    //Standtable
    QTableView standTable;
    QStandardItemModel standModel(0, 4 );

    standModel.setHeaderData(0, Qt::Horizontal, QObject::tr("Datum"));
    standModel.setHeaderData(1, Qt::Horizontal, QObject::tr("Mannschaft"));
    standModel.setHeaderData(2, Qt::Horizontal, QObject::tr("Gegner"));
    standModel.setHeaderData(3, Qt::Horizontal, QObject::tr("Ergebnis"));


    insertRow.setEditable(false);

    standTable.setModel(&standModel);
    standModel.insertRow(0, insertRow.clone());


    QApplication::connect(&standTable, &QTableView::doubleClicked,
            [&standModel](const QModelIndex index)
    {
        int rowCount = standModel.rowCount();
        if (index.row() == rowCount - 1)
            standModel.insertRow(rowCount - 1, new QStandardItem{ "New item" });
    });
    //Standtable





        // var für neue row muss aus der expression gesetzt werden warum auch immer..
        QStandardItem insertr("test");


    //Lambda Expression wenn im ersten fenster doppelgeclickt wird wird das 2te fenster sichtbar
    QApplication::connect(&view1, &QTableView::doubleClicked,
            [&view1,&personTable,&personModel,&model1,&insertr,&zeitTable,&zeitModel,&standTable,&standModel]
    {


        //umwandlung in text   (sehr muehsam !?)
        ipeing = model1.item(0,1)->text().toUtf8().constData();
        usereing = model1.item(0,2)->text().toUtf8().constData();
        dbeing = model1.item(0,4)->text().toUtf8().constData();
        porteing = model1.item(0,5)->text().toUtf8().constData();
        passworteing = model1.item(0,3)->text().toUtf8().constData();


       //params : connectdb(string ipeing, string usereing, string passworteing, string porteing, string dbeing)
      thread startcon(connectdb);
      startcon.join();
      view1.hide();
      thread getres(getdaten,"SELECT * from Spieler");
      getres.join();


   // einlesen in die personentabelle




   const int num_rows = res.size();
   for (int rownum=0; rownum < num_rows; ++rownum)
   {


     personModel.insertRow(0,insertr.clone());
     const pqxx::tuple row = res[rownum];
     const int num_cols = row.size();

     for (int colnum=0; colnum < num_cols; ++colnum)
     {
       const pqxx::field field = row[colnum];
       QString in = field.c_str();
       QStandardItem inhalt(in);

       if(colnum==0){
           inhalt.setEditable(false);
       }

       personModel.setItem(0,colnum,inhalt.clone());

     }

   }

      personTable.show();
   // einlesen in die personentabele ende


      // einlesen in die zeittabele


      thread getreszeit(getdaten,"SELECT * from beteiligt");
      getreszeit.join();


      const int num_rowszeit = res.size();
      for (int rownum=0; rownum < num_rowszeit; ++rownum)
      {


        zeitModel.insertRow(0,insertr.clone());
        const pqxx::tuple row = res[rownum];
        const int num_cols = row.size();

        for (int colnum=0; colnum < num_cols; ++colnum)
        {
          const pqxx::field field = row[colnum];
          QString in = field.c_str();
          QStandardItem inhalt(in);

          if(colnum==0||colnum==1||colnum==2){
              inhalt.setEditable(false);
          }

          zeitModel.setItem(0,colnum,inhalt.clone());

        }

      }

         zeitTable.show();
      // einlesen in die zeittabele ende




         // einlesen in die standtabele
         thread getresstand(getdaten,"SELECT * from Aufzeichnung");
         getresstand.join();


         const int num_rowsstand = res.size();
         for (int rownum=0; rownum < num_rowsstand; ++rownum)
         {


           standModel.insertRow(0,insertr.clone());
           const pqxx::tuple row = res[rownum];
           const int num_cols = row.size();

           for (int colnum=0; colnum < num_cols; ++colnum)
           {
             const pqxx::field field = row[colnum];
             QString in = field.c_str();
             QStandardItem inhalt(in);

             if(colnum==0||colnum==1||colnum==2){
                 inhalt.setEditable(false);
             }

             standModel.setItem(0,colnum,inhalt.clone());

           }

         }

            standTable.show();
         // einlesen in die standtabele ende




    });






    return a.exec();
}


